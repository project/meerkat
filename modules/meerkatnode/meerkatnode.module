<?php
// $Id$

/**
 *  @file
 *  Provides the jquery meerkat plugin functionality for nodes.
 */

/**
 * Implements hook_menu().
 */
function meerkatnode_menu() {
  $items = array();

  $items['admin/config/meerkat/meerkatnode'] = array(
    'title' => 'Meerkat Node',
    'description' => 'Administer meerkat node settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('meerkatnode_admin_settings'),
    'access arguments' => array('administer meerkat'),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * The meerkat node admin settings form.
 */
function meerkatnode_admin_settings($form, &$form_state) {
  $types = node_type_get_names();

  $form['meerkatnode_types'] = array(
    '#type' => 'checkboxes',
    '#required' => TRUE,
    '#title' => t('Meerkat Content Types'),
    '#options' => $types,
    '#description' => t('Please select which content types to enable meerkat node functionality on.'),
    '#default_value' => meerkatnode_types(),
  );

  return system_settings_form($form);
}

/**
 * Returns the node types configured for meerkat node functionality.
 */
function meerkatnode_types() {
  $default = array();

  $types = node_type_get_names();

  $selectedtypes = array_filter(variable_get('meerkatnode_types', $default));

  $validtypes = array();

  if ( is_array($selectedtypes) ) {
    foreach ( $selectedtypes as $selectedtype ) {
      if ( isset($types[$selectedtype]) ) {
        $validtypes[] = $selectedtype;
      }
    }
  }

  return $validtypes;
}

/**
 * Implements hook_form_alter().
 */
function meerkatnode_form_alter(&$form, $form_state, $form_id) {
  if ( strstr($form_id, '_node_form') ) {
    $node = $form['#node'];

    if ( is_object($node) ) {
      $types = meerkatnode_types();

      if ( in_array($node->type, $types) ) {
        _meerkatnode_form($form);
      }
    }
  }
}

/**
 * Injects the meerkat node form.
 */
function _meerkatnode_form(&$form) {
  $node = $form['#node'];

  $form['meerkatnode'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Meerkat'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );

  $content = '';

  if ( isset($node->meerkatnode['content']) ) {
    $content = $node->meerkatnode['content']['value'];
  }

  $form['meerkatnode']['content'] = array(
    '#type' => 'text_format',
    '#basetype' => 'textarea',
    '#title' => t('Meerkat Content'),
    '#description' => t('Please enter the content to display in the meerkat container.'),
    '#default_value' => $content,
    '#format' => isset($form['format']) ? $form['format'] : NULL,
  );


  $options = meerkat_defaultsoptions();

  if ( count($options) == 0 ) {
    $form['meerkatnode']['defaults'] = array(
      '#type' => 'value',
      '#value' => '',
    );
  }
  else {
    $form['meerkatnode']['defaults'] = array(
      '#type' => 'select',
      '#title' => t('Defaults'),
      '#options' => $options,
      '#description' => t('Please select which meerkat defaults to use.'),
      '#default_value' => $node->meerkatnode['defaults'],
    );
  }
}

/**
 * Stores the meerkat node data.
 */
function meerkatnode_node_insert($node) {
  $types = meerkatnode_types();
  if ( in_array($node->type, $types) ) {
   $insert = db_insert('meerkatnode')
    ->fields(array(
      'nid' => $node->nid,
      'vid' => $node->vid,
      'data' => serialize($node->meerkatnode),
    ))
    ->execute();
  }
}

/**
 * Loads the meerkat node data.
 */
function meerkatnode_node_load($nodes, $types) {
  $meerkat_node_types = meerkatnode_types();
  $vids = array();
  
  foreach ($nodes as $node) {
    // We are using the revision id instead of node id
    if (in_array($node->type, $meerkat_node_types)) {
      $vids[] = $node->vid;
    }
  }
  // When we read, we don't care about the node->nid, but we look for the right
  // revision ID (node->vid)
  $result = db_select('meerkatnode', 'm')
    ->fields('m', array(
      'nid',
      'vid',
      'data',
    ))
    ->where('m.vid IN (:vids)', array(':vids' => $vids))
    ->execute();

  foreach ($result as $record) {
    $nodes[$record->nid]->meerkatnode = unserialize($record->data);
  }
}


/**
 * Updates the meerkat node data.
 */
function meerkatnode_node_update($node) {
  $result = db_update('meerkatnode')
    ->fields(array(
      'data' => serialize($node->meerkatnode),
    ))
    ->condition('vid', $node->vid)
    ->execute();

// dpm($result);

  // if ( $result->rowCount() == 0 ) {
  //   meerkatnode_node_insert($node);
  // }
}

/**
 * Deletes the meerkat node data for a entire node.
 */
function meerkatnode_node_delete(&$node) {
  db_delete('meerkatnode')
    ->condition('nid', $node->nid)
    ->execute();
}

/**
 * Deletes the meerkat node data for a node revision.
 */
function _meerkatnode_deleterevision(&$node) {
  // $sql = "DELETE FROM {meerkatnode} WHERE vid = %d";

  // TODO Please review the conversion of this statement to the D7 database API syntax.
  /* db_query($sql, $node->vid) */
  db_delete('meerkatnode')
  ->condition('vid', $node->vid)
  ->execute();
}

/**
 * Outputs the meerkat node data.
 */
function meerkatnode_node_view($node) {
  if ( arg(0) == 'node' && arg(1) == $node->nid && isset($node->meerkatnode) && $node->meerkatnode['content'] != '' ) {
    $content = check_markup($node->meerkatnode['content']['value'], $node->meerkatnode['content']['format']);

    meerkat_create($content, $node->meerkatnode['defaults']);
  }
}
